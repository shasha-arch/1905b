# Git
Git是一个开源的分布式版本控制系统，可以有效、高速地处理从很小到非常大的项目版本管理。

## git三大分区：（工作区，暂存区，版本库）

## 配置命令
当我们需要与远程仓库的账户连接时，需要配置用户的信息
 配置用户名
 `git config --global user.name "your name"   `
 配置密码
 `git config --global user.email "your email @github.com"`

## 分支管理

### 查看分支
查看本地分支
`git branch `
 查看远程分支
 `git branch -r`
 查看全部分支
 `git branch -a`
### 切换分支
 从当前分支切换到指定分支
 `git checkout <branch name> `
 创建并切换到新的分支
 `git checkout -b <branch name>` 
 拉取远程分支并切换到本地分支
 `git checkout -b 本地分支名 origin/远程分支名 `
### 合并分支
 当前分支与指定分支合并
`git merge <branch name>`
### 拉取分支
 拉取远程分支
 `git pull origin 远程分支名`
 强制拉取分支
 `git pull origin master --allow-unrelated-historie `
### 查看历史记录
`git log`
### 查看状态
`git status`
### 暂存
 暂存
 `git stash` 
 返回暂存
 `git stash pop`
### 关联
 关联仓库
 `git remote add origin 地址` 
 查看关联状态
 `git remote -v `

## git合并步骤：
(1)克隆代码与远程分支进行链接 [git clone 地址]
(2)创建本地developer与远程developer进行链接  [git checkout -b developer login/developer]
(3)创建个人分支与远程分支进行链接   [git checkout -b 个人  login/个人]
(4)拉取developer的代码   [git pull origin developer]
(5)更新个人代码并提交   [git add / git commit -m /git push]
(6)转到developer分支进行代码合并  [git checkout developer /  git pull / git merge 个人分支 / git add / git commit  /git push]





