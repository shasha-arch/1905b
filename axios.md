# axios二次封装

## axios二次封装
```
axios.create({
  baseURL: 'https://some-domain.com/api/',
  timeout: 1000,   //每隔一秒请求一次
  headers: {'X-Custom-Header': 'foobar'}
});
axios.interceptors.request.use(function (config) {
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

axios.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    //接受状态码，来判断axios请求的状态
    let err=error.response.status;
    switch (err){
        case "404":
          alert("404 - 请求的资源（网页等）不存在");
          break;
        case "500":
          alert("500 - 内部服务器错误");
          break;
        case "401":
          alert("404 - 没有访问权限");
          break;
        case "404":
          alert("404 - 请求的资源（网页等）不存在");
          break;
    }
    return Promise.reject(error);
  });
```
